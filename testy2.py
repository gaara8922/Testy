import unittest
from unittests_functions_v2 import is_even, square, is_palindrom, is_prime 
from unittests_functions_v2 import add_dot, count_letters, multiply_by_2

class PyCalc(object):

    def add(self, a, b):
        return a + b

class PyCalcTestCase(unittest.TestCase):

    def test_add(self):
        py_calc = PyCalc()
        self.assertEqual(py_calc.add(2, 3), 5)

    def test_is_even(self):
        for i in xrange(0,100,2):
            self.assertEqual(is_even(i), True)
        for i in xrange(1,101,2):
            self.assertEqual(is_even(i), False)
        #self.assertEqual(is_even(0), True)
        #self.assertEqual(is_even(1 ), False)
        #self.assertEqual(is_even(2 ), True)
        #self.assertEqual(is_even(3 ), False)
        #self.assertEqual(is_even(10 ), True)
        #self.assertEqual(is_even(11 ), False)

    def test_square(self):
        self.assertEqual(square(0 ), 0)
        self.assertEqual(square(1 ), 1)
        self.assertEqual(square(2 ), 4)
        self.assertEqual(square(5 ), 25)

    def test_is_palindrom(self):
        self.assertEqual(is_palindrom(''), True)
        self.assertEqual(is_palindrom('a'), True)
        self.assertEqual(is_palindrom('kajak' ), True)
        self.assertEqual(is_palindrom('home' ), False)

    def test_is_prime(self):
        self.assertEqual(is_prime(0 ), False)
        self.assertEqual(is_prime(1 ), False)
        self.assertEqual(is_prime(2 ), True)
        self.assertEqual(is_prime(3 ), True)
        self.assertEqual(is_prime(4 ), False)       #tu error
        self.assertEqual(is_prime(5 ), True)
        self.assertEqual(is_prime(7 ), True)

    def test_add_dot(self):
        self.assertEqual(add_dot( ), 'example sentence .')
        self.assertEqual(add_dot( ), 'example sentence .')    # eror bo jest . .
        #self.assertEqual(add_dot( ), 'example sentence . .')    
        self.assertEqual(add_dot(['home'] ), 'home .')
        self.assertEqual(add_dot(['home'] ), 'home .')

    def test_count_letters(self):
        self.assertEqual(count_letters(['home'] ),  {'home': 4})
        #self.assertEqual(count_letters('home' ),  4) dla moj _v1
        
    def test_multiply_by_2(self):
        self.assertEqual(multiply_by_2(5 ), 10)
        self.assertEqual(multiply_by_2(10), 20)
        self.assertEqual(multiply_by_2(1 ), 2)
        self.assertEqual(multiply_by_2(0 ), 0)



