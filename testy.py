import unittest
from unittests_functions_v1 import is_even, square, is_palindrom, is_prime 
from unittests_functions_v1 import add_dot, count_letters, multiply_by_2

class PyCalc(object):
    def add(self, a, b):
        return a + b

class PyCalcTestCase(unittest.TestCase):

    def test_add(self):
        py_calc = PyCalc()
        self.assertEqual(py_calc.add(2, 3), 5)

    def test_is_even(self):
        self.assertEqual(is_even(10 ), True)

    def test_square(self):
        self.assertEqual(square(5 ), 25)

    def test_is_palindrom(self):
        self.assertEqual(is_palindrom('kajak' ), True)

    def test_is_prime(self):
        self.assertEqual(is_prime(5 ), True)

    def test_add_dot(self):
        self.assertEqual(add_dot( ), 'example sentence .')
        self.assertEqual(add_dot(['home'] ), 'home .')
    
    def test_count_letters(self):
        self.assertEqual(count_letters(['home'] ),  {'home': 4})
    
    def test_multiply_by_2(self):
        self.assertEqual(multiply_by_2(5 ), 10)



